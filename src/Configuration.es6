
// Configuration es para representar Configuraciones a partir de archivos
import Path from 'path'
import {EventEmitter} from 'events'
var Fs= core.System.IO.Fs
var D= core.org.voxsoftware.Developing

class Configuration extends EventEmitter{



	static require(module){


		var ext= Path.extname(module)
		if(ext==".dt"){
			return (new D.Formats.DataText()).read(module)
		}

		Configuration.cache= Configuration.cache||{}
		var file= require.resolve(module)
		if(!file)
			throw new core.System.IO.FileNotFoundException(file)

		var stat= Fs.sync.stat(file)
		var c= Configuration.cache[file]= Configuration.cache[file]||{}
		if(stat.mtime.getTime()!= c.mtime){
			delete require.cache[file]
		}
		c.mtime= stat.mtime.getTime()
	    return require(file)
	}

	
	constructor(fileOrJson, parent){

		super()
		this.$= this.$ ||{}
		if(typeof fileOrJson == "string"){

			var stat= Fs.sync.stat(fileOrJson)
			if(stat.isDirectory())
				fileOrJson= Path.join(fileOrJson, this.defaultFileName||"configuration.json")



			this.$.isDt= fileOrJson.endsWith(".dt")
			this.$.json= Configuration.require(fileOrJson)
			if(this.$.json.default)
				this.$.json= this.$.json.default
			this.$.file= fileOrJson
		}
		else{
			this.$.json= fileOrJson
		}

		this.$.parent= parent

		/*
		for(var id in this.$.json){
			this.createGetter(id)
			this.createSetter(id)
		}*/


	}


	createGetter(id){
		this.__defineGetter__(id, ()=>{
			return this.get(id)
		})
	}

	createSetter(id){
		this.__defineSetter__(id, (val)=>{
			return this.set(id,val)
		})
	}


	get(name){
		var j= this.$.json
		var i, parts= name.split(".")
		i=0

		while(j && i<parts.length){
			if(j instanceof Configuration)
				j= j.get(parts[i])
			else
				j=j[parts[i]]
			i++
		}

		return j
	}


	set(name, val){

		var j= this.$.json
		var parts= name.split("."), part

		for(var i=0;i<parts.length;i++){
			part= parts[i]
			if(i==parts.length-1)
				j[part]= val
			else if(!j[part])
				j[part]= {}
			j= j[part]
		}
		this.$edited= true
		return this
	}


	get parent(){
		return this.$.parent
	}

	get root(){
		var root= null, p= this
		while(p){
			root= p
			p= p.parent
		}
		return root
	}

	get filename(){
		return this.$.file
	}

	get directoryName(){
		if(!this.$.dir){
			//vw.log(this.$)
			this.$.dir= Path.dirname(this.filename)
		}

		return this.$.dir
	}


	toJSON(){
		return this.$.json
	}



	save(){

		if(this.$.isDt){
			return D.Formats.DataText.save(this)
		}

		var file= this.filename
		var p= this.parent, lp
		if(!file){
			while(!file && p){
				file= p.filename
				if(file)
					lp=p
				p= p.parent
			}

			if(lp)
				return lp.save()

		}

		if(!file)
			throw new core.System.Exception("No se encontró el archivo de configuración")

		var data= core.safeJSON.stringify(this, null, '\t')
		if(file.endsWith(".es6") || file.endsWith(".js")){
			data= "module.exports= " + data
		}
		else if(!file.endsWith(".json")){
			throw new core.System.NotImplementedException("El formato del archivo" + file + " no es soportado para guardar.")
		}
		Fs.sync.writeFile(file, data)
		return this
	}


}

export default Configuration
