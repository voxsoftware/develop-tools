import Path from 'path'
var D= core.org.voxsoftware.Developing
class Screen extends D.Project.Image{


	static get landscape(){

		return new Screen({
			"path": Path.join(__dirname, "..", "..", "design", "screen-land.png"),
			"size":{
				width: 1920,
				height: 1080
			}
		})
	}

	static get portrait(){
		return new Screen({
			"path": Path.join(__dirname, "..", "..", "design", "screen-port.png"),
			"size":{
				width: 1080,
				height: 1920
			}
		})

	}



}

export default Screen
