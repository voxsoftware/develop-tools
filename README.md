# Develop-tools

Develop-tools es un conjunto de archivos (pequeños módulos) que facilitan algunas tareas comunes en la creación de proyectos. 

Tiene minimódulos para: 

- Está enfocado para usar compilaciones automatizadas con [vox-webcompiler](https://www.npmjs.com/package/vox-webcompiler)
- Manejo de archivos .ignore
- Copiar archivos de un lugar a otro de manera automatizada
- Compilación de archivos .es6.html formato template de [vox-core](https://www.npmjs.com/package/vox-core)
- Manejar archivos de configuración en formato .json, .es6 o .dt



### Vox-platform

Develop-tools también es parte del proyecto aún en desarrollo vox-platform (aún no disponible link) el cual es una plataforma de creación de aplicaciones móviles (inicialmente android, luego ios) usando JxCore un reemplazo de NOdeJs v0.12 en plataformas móviles.


