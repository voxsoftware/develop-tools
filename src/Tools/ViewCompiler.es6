

// Este archivo facilitará las tareas de compilación
// de los views a HTML ...

// Automáticamente llevará control de los archivos
// que han sido modificados y se añadirá a la configuración para exportar




import Path from 'path'
var Fs= core.System.IO.Fs
import ExtractTextPlugin from "vox-webcompiler/node_modules/extract-text-webpack-plugin"
import Os from 'os'

class ViewCompiler{

    constructor(path){
        this.$= {}
        this.$.path= path
        this.$.dbFile= Path.join(path,"db.json")

        if(Fs.sync.exists(this.$.dbFile))
            this.$.db= require(this.$.dbFile)
        else
            this.$.db={}
    }


    get(name){
        return this.$.db[name]
    }

    set(name, val){
        this.$.db[name]= val
    }




    save(){
        Fs.sync.writeFile(this.$.dbFile, JSON.stringify(this.$.db, null, '\t'))
    }



    verify(src, stat, dest){

        // Verificar si el archivo fue cambiado ...
        var ctime, mtime, ctime2, mtime2, stat2


		// ESto verifica si se debe ignorar o no ...
		// Por ahora se hace de forma simple
		// después se manejará archivos .ignore ...
		var e= this.get("ignores"), p
		e= e|| []
		p= Path.relative(this.$.path, src)


		if(Os.platform()=="win32"){
			while(p.indexOf("\\")>=0){
				p= p.replace("\\", "/")
			}
		}

		//vw.log(p)
		if(e.indexOf(p)>=0)
			return  false


		return true

		/*

        if(!Fs.sync.exists(dest))
            return true

        stat2= Fs.sync.stat(dest)

        ctime= stat.ctime.getTime()
        mtime= stat.mtime.getTime()

        ctime2= stat2.ctime.getTime()
        mtime2= stat2.mtime.getTime()
        return (ctime2< ctime || mtime2< mtime)

		*/

    }


	_añadir(src, dest){
		var plugin= this.createExtractPlugin(dest)
		var e= ViewCompiler.getDefaultConfig(plugin)
		e.entry= src
		e.output.path= Path.dirname(dest)
		this.$.config.push(e)
	}


	process(dest){
		this.$.config=[]
		//this.$.relateTo= path

		core.org.voxsoftware.Developing.Tools.Es6HtmlLoader.internal_resource= true
		core.org.voxsoftware.Developing.Tools.Es6HtmlLoader.setFolder(this.$.path)
		return this._revisar(this.$.path, dest)

	}




	getCompileConfig(){
		return this.$.config
	}

	createExtractPlugin(out){

		var extract= new ExtractTextPlugin(Path.basename(out))
		return {
			loader: extract.extract( Path.join(__dirname, "es6-html-loader.es6") ),
			plugin: extract
		}
	}


	static getDefaultConfig(extract){


		var data = {
	    	node: {
	    	    "fs": "empty"
	    	},
	    	module: {
	        	loaders: [
	        		{ test: /\.json$/, loader: "json-loader"},
	                { test: /\.es6.html$/, loader:  extract.loader }
	        	]
	      	},
	        entry:  null,
	        output: {
	            path:  "",
	            filename: "temp.js"
	        },
	        plugins:[
	            extract.plugin
	        ]
	    }

		return data

	}


    _revisar(path, dest){

        var files= Fs.sync.readdir(path), file, cfile, cdes, fname, plugin
        for(var i=0;i<files.length;i++){
            file= files[i]
            cfile= Path.join(path, file)
            stat= Fs.sync.stat(cfile)
            fname= Path.basename(file, ".es6.html")

            cdest= Path.join(dest, fname)
            if(stat.isDirectory()){
                this._revisar(cfile, cdest)
            }
            else{
				if(file.endsWith(".es6.html")){
	                cdest+= ".html"
	                if(this.verify(cfile, stat, cdest))
						this._añadir(cfile, cdest)
				}
            }
        }

    }


}


export default ViewCompiler
