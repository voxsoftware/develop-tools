
import Path from 'path'
var Fs= core.System.IO.Fs
class Es6Loader{


	static async awaitTranspiler(transpiler){
		var task= new core.VW.Task(), er
		transpiler.compileTask= task
		try{
			await transpiler.compileTask
		}
		catch(e){
			task.exception= e
			er= e
		}
		task.finish()

		if(er)
			throw er
	}


	static async process(){

		var callback= this.async()

		try{


			var transpiler= Es6Loader.currentTranspiler
			if(!transpiler.compileTask.isCompleted){
				await Es6Loader.awaitTranspiler(transpiler)
			}

			var src= this.resource
			var file1= transpiler.archivosTotales[src]
			var code= "module.exports= require("+JSON.stringify(file1)+")"
			//vw.info(src, transpiler.archivosTotales	)

			//vw.info(transpiler.lastDest, transpiler.archivosTotales[src])
			// Retornar el contenido del archivo transpilado ...
			callback(null, code)
			
		}
		catch(e){
			callback(e)
		}

	}

}


export default Es6Loader
