
var D= core.org.voxsoftware.Developing
class Project extends D.Configuration{


	get defaultFileName(){
		return "vox.project"
	}



	get name(){
		return this.get("name")
	}

	set name(val){
		return this.set("name", val)
	}

	get id(){
		return this.get("id")
	}

	set id(val){
		return this.set("id", val)
	}


	get outPath(){
		return this.get("out.path")
	}

	set outPath(path){
		return this.set("out.path", path)
	}




	get outputs(){

		var releases= this.get("out.releases"), release
		if(! (releases instanceof Array)){
			releases=[]
			this.set("out.releases", releases)
		}
		for(var i=0;i<releases.length;i++){
			release= releases[i]
			if(!(release instanceof D.Project.Output)){
				release= new D.Project.Output(release, this)
				releases[i]= release
			}
		}
		return releases

	}

	getOutputFor(platform){
		var outputs= this.outputs
		//vw.log(outputs)
		for(var i=0;i<outputs.length;i++){
			if(outputs[i].platform==platform)
				return outputs[i]
		}
	}



}

export default Project
