import Path from 'path'
var D= core.org.voxsoftware.Developing
class Icon extends D.Project.Image{


	static get ["default"](){

		return new Icon({
			"path": Path.join(__dirname, "..", "..", "design", "icon.png"),
			"size":{
				width: 256,
				height: 256
			}
		})

	}



}

export default Icon
